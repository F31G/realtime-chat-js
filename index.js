const express = require('express')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
let users = [];
const PORT = 3005

// mongoose Using Node.js `require()`
const mongoose = require('mongoose');

// On sert le dossier public
app.use(express.static('public'));

// On se sert de socket IO pour emettre / diffuser des événéments
io.on('connection', (socket) => {

    // On garde l'id du user
    const userId = socket.id

    // Ici: renvoyer l'historique stocké dans mongo ?

    // Sur l'action d'une nouvelle connextion d'un utilisateur
    socket.on('newUser', (username) => {



        // On le rajoute à la liste des utiliseurs
        users.push({ id: userId, username: username });

        // On prévient tout le monde de son arrivé
        io.emit('newNotification', username + ' vient de se connecter');

        // On demande à tous le monde de mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });

    // Sur l'action d'un nouveau message reçu
    socket.on('newMessage', (username, msg, date) => {

        // Ici: stocker dans mongo ? 

        // On diffuse le message à tout le monde
        io.emit('newMessage', username, msg, date);
    });

    // Sur l'action d'une déconnextion d'un utilisateur
    socket.on('disconnect', () => {

        // On cherche le user dans la liste pour le retirer
        users = users.filter(user => {
            return user.id != userId;
        });

        // On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });


    //socket.on('nouvelle couleur blabla', jeChangeDecouleur());
});
//function jeChangedeCouleur()

http.listen(PORT, () => {
    console.log('listening on http://localhost:' + PORT);
});

// est-ce que userName est dèja existant ?
// users.forEach(element)

// socket.emit

// socket.on('newUser')

// if newUser = contain users
// no accept neexistwUsername
// requestUsername