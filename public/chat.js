const socket = io()

let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
const formalize = "<br/>" + "<i>"


// Get users list
const usersList = document.getElementById('users')


// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}


// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function(e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;

        // Date et heure du message

        //////// const date = new Date();


        const date = moment().format('LLL'); // 2 février 2021 16:35

        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    const item3 = document.createElement('strong')
    item3.textContent = '  ' + username
    const item2 = document.createElement('span')
    item2.textContent = ' - ' + date + ' :';
    const item = document.createElement('li')
    item.textContent = ' ' + msg
        //     item.textContent = username + ': ' + msg + "<br/>" + "<i>" + ' - ' + date + "</i>"

    messages.appendChild(item3)
    messages.appendChild(item2)
    messages.appendChild(item)


    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notification
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');
    users.forEach(user => {
        const li = document.createElement('li')
        li.textContent = user.username
        usersList.appendChild(li)
    })
})